import cv2
import numpy as np
import math

#--------------------------------------------------------------------------------------
def mask(img,mask):
	ret = np.zeros(img.shape[0:2],np.uint8);
	idx = (mask!=0);
	ret[idx] = src[idx];
	return ret

#--------------------------------------------------------------------------------------
def createThresold(img,rangeMin,rangeMax):
	ret,zero = cv2.threshold(img,rangeMin,rangeMax,cv2.THRESH_TOZERO);
	ret,otsu = cv2.threshold(img,0,255,cv2.THRESH_OTSU);
	zero = cv2.bitwise_not(zero);
	otsu = cv2.bitwise_not(otsu);
	result = cv2.add(zero,otsu);
	ret,final = cv2.threshold(result,127,255,cv2.THRESH_TOZERO);
	return final;
#--------------------------------------------------------------------------------------	
def colorize(img):
	return cv2.cvtColor(img, cv2.COLOR_GRAY2RGB);
	
#--------------------------------------------------------------------------------------
def applieMask(img,mask):
	mask_werk = mask.copy();
	mask_werk = cv2.bitwise_not(mask_werk);
	return cv2.subtract(img,mask_werk);

#--------------------------------------------------------------------------------------
#DRAWING CONTOURS OF A CERTAIN PATICLE IN AN IMAGE
def drawContourOn(img,particlePlus):
	contour = particlePlus['contours'];
	imRet = img.copy();
	cv2.drawContours(imRet,contour,-1,(233,17,167),2);
	x = particlePlus['propertys'][18];
	y = particlePlus['propertys'][19];
	cv2.circle(imRet,(int(x),int(y)),3,(0,0,255),-1);
	return imRet;
	
#--------------------------------------------------------------------------------------
#GET EVERY PARTICLE IN AN IMAGE
def getParticles(partImg,img):
	img_work = img.copy();
	contours, hier = cv2.findContours(img_work,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE);
	particles = [];
	index = 0;
	for cnt in contours:
		part = pProp(partImg,img_work,cnt,index);
		particles.append(part);
		index = index+1;
	
	return particles;
	
#--------------------------------------------------------------------------------------
#SHOW A SPECIFIC PARTILE
def showParticle(particlePlus):
	particle = particlePlus['propertys'];
	
	print "";
	print "PARTICLE",particlePlus['name'];	
	print " -perimeter :",particle[0];
	print " -area :",particle[1];
	print " -roundness :",particle[2];
	print " -compactness :",particle[3];
	print " -rc :",particle[4];
	print " -minDistance :",particle[5];
	print " -maxDistance :",particle[6];
	print " -mean :",particle[7];
	print " -ratio1 :",particle[8];
	print " -ratio2 :",particle[9];
	print " -ratio3 :",particle[10];
	print " -diameter :",particle[11];
	print " -radiusDispersion :",particle[12];
	print " -holes :",particle[13];
	print " -eN2 :",particle[14];
	print " -rms_min :",particle[15];
	print " -mean_distance_to_boundary :",particle[16];
	print " -complexity :",particle[17];
	print " -centerX :",particle[18];
	print " -centerY :",particle[19];
	print " -colorAverage :",particle[20];
	
def showParticleImage(particlePlus,img):
	showParticle(particlePlus);
	cont = drawContourOn(img,particlePlus);
	cv2.imshow(particlePlus['name'],cont);
	
#--------------------------------------------------------------------------------------
#
def pProp(partImg,img,contour,name):
	name = str(name);
	print "creating particle '"+name+"'";
	#create empty feature list
	particle = np.zeros(21);
	cnt = contour;
	final = img.copy();
	
	#create black image with contours
	imageContours = np.zeros(final.shape[0:2],np.uint8) 
	cv2.drawContours(imageContours,cnt,-1,(255,255,255),1);

	#length of contours in pixels
	contourPointList = np.transpose(np.nonzero(imageContours))
	number_of_points = np.shape(contourPointList)
	lengthOfContour = len(cnt);

	#calculate perimeter
	perimeter = getPerimeter(cnt)
	particle[0] = perimeter;

	#calculate area
	area = cv2.contourArea(cnt)
	particle[1] = getArea(cnt);

	#calculate roundness
	roundness = getRoundness(area,perimeter);
	particle[2] = roundness;
		 
	#calculate compactness (1/roundness)
	compactness = getCompactness(roundness);
	particle[3] = compactness;

	#calculate rc -- related to roundness
	rc = getRc(area,perimeter);
	particle[4] = rc
	
	#calculate centroid
	M = cv2.moments(cnt)
	if(M['m00']>0 and M['m00']>0):
		centerX = int(M['m10']/M['m00']);
		centerY = int(M['m01']/M['m00']);
		centroid = [centerX, centerY];
	else:
		centroid = [0, 0];
	particle[18] = centroid[0];
	particle[19] = centroid[1];
	
	#calculate minDistane and maxDistance
	min_distance = 1000000
	max_distance = 0
	
	current_distance = 0
	sum_distances = 0 
	point_counter = 0
	
	for each in contourPointList:
		current_distance = getDistance(each,centroid)
		if current_distance < min_distance:
			min_distance = current_distance;
		if current_distance > max_distance:
			max_distance = current_distance;
		sum_distances = current_distance + sum_distances;
		point_counter = point_counter +1;
	mean_distance = sum_distances/len(cnt);

	particle[5] = min_distance
	particle[6] = max_distance
	particle[7] = mean_distance
	
	#calculate ratio1 & 2
	ratio1,ratio2 = getRatio12(particle[5],particle[6]);
	particle[8] = ratio1;
	particle[9] = ratio2;
	
	#calculate ratio3
	ratio3 = getRatio3(particle[5],particle[7]);
	particle[10] = ratio3;
	
	#calculate diamater
	diameter = getDiameter(contourPointList);
	particle[11] = diameter;
	
	#calculate radiusDispersion
	radiusDispersion = getRadiusDispersion(contourPointList,centroid,particle[7]);
	particle[12] = radiusDispersion;
	
	#calculate holes
	holes = getHoles(contourPointList,centroid,particle[6]);
	particle[13] = holes;
	
	#calculate eN2
	eN2,rms_mean = get2ndEuclideanNorm(contourPointList,centroid);
	particle[14] = eN2;
	particle[15] = rms_mean;
	
	#calculate mean_distance_to_boundary
	area_copy = final.copy();
	border = imageContours.copy();
	
	area_no_border = (area_copy - border)
	inner_area_points = np.transpose(np.nonzero(area_no_border))

	sum_distances = 0 
	point_counter = 0

	for each in inner_area_points:
		min_distance = 1000000
		current_distance = 0
		for borderpoint in contourPointList:
			current_distance = getDistance(each,borderpoint)
			if current_distance < min_distance:
				min_distance = current_distance
		sum_distances = min_distance + sum_distances
		point_counter = point_counter +1
	mean_distance_to_boundary = sum_distances/len(inner_area_points)
	test = sum_distances/point_counter 

	particle[16] = mean_distance_to_boundary;
	
	#calculate complexity
	complexity = getComplexity(area,particle[16]);
	particle[17] = complexity;
	
	radius = (particle[11])/2;
	x1 = particle[18]-radius;
	y1 = particle[19]-radius;
	x2 = particle[18]+radius;
	y2 = particle[19]+radius;
	if(x1<0):
		x1=0;
	if(y1<0):
		y1=0;
	imageParticle = partImg[y1:y2, x1:x2];
	
	particle[20] = getMeanColor(imageParticle);
	
	particlePlus = {'propertys':particle,'contours':cnt,'name':"particle "+str(name),'image':imageParticle};
	
	return particlePlus;
#MATHS --------------------------------------------------------------------------------------
def getPartPerimeter(particlePlus):
	return particlePlus['propertys'][0];
def getPartArea(particlePlus):
	return particlePlus['propertys'][1];
def getPartRoundness(particlePlus):
	return particlePlus['propertys'][2];
def getPartCompactness(particlePlus):
	return particlePlus['propertys'][3];
def getPartRc(particlePlus):
	return particlePlus['propertys'][4];
def getPartMinDist(particlePlus):
	return particlePlus['propertys'][5];
def getPartMaxDist(particlePlus):
	return particlePlus['propertys'][6];
def getPartMean(particlePlus):
	return particlePlus['propertys'][7];
def getPartRatio1(particlePlus):
	return particlePlus['propertys'][8];
def getPartRatio2(particlePlus):
	return particlePlus['propertys'][9];
def getPartRatio3(particlePlus):
	return particlePlus['propertys'][10];
def getPartDiameter(particlePlus):
	return particlePlus['propertys'][11];
def getPartRadiusDispersion(particlePlus):
	return particlePlus['propertys'][12];
def getPartHoles(particlePlus):
	return particlePlus['propertys'][13];
def getPartEN2(particlePlus):
	return particlePlus['propertys'][14];
def getPartRms_min(particlePlus):
	return particlePlus['propertys'][15];
def getPartMean_Dit_To_Boundary(particlePlus):
	return particlePlus['propertys'][16];
def getPartComplexity(particlePlus):
	return particlePlus['propertys'][17];
def getPartCenterX(particlePlus):
	return particlePlus['propertys'][18];
def getPartCenterY(particlePlus):
	return particlePlus['propertys'][19];
def getPartColorAverage(particlePlus):
	return particlePlus['propertys'][20];
#MATHS --------------------------------------------------------------------------------------
def getPerimeter(cnt):
	return cv2.arcLength(cnt,True);
	
def getArea(cnt):
	return cv2.contourArea(cnt);
	
def getRoundness(area,perimeter):
	if(perimeter>0):
		return 4*math.pi*(area/(perimeter)*(perimeter));
	else:
		return 0;
	
def getCompactness(roundness):
	if(roundness>0):
		return 1/roundness;
	else:
		return 0;
	
def getRc(area,perimeter):
	if(area>0 and perimeter>0):
		return (perimeter-math.sqrt(perimeter*perimeter-4*math.pi*area))/(perimeter+math.sqrt(perimeter*perimeter-4*math.pi*area));
	else:
		return 0;
	
def getDistance(first_point,second_point):
	dx = abs(first_point[0] - second_point[0])
	dy = abs(first_point[1] - second_point[1])
	result = math.sqrt(dx*dx+dy*dy)
	return result
	
def getRatio12(min_distance,max_distance):
	if min_distance != 0:
		return (min_distance,min_distance/max_distance);
	else:
		return (0,0);

	
def getRatio3(min_distance,mean_distance):
	if mean_distance != 0:
		return min_distance/mean_distance
	else:
		return 0
	
	
def getDiameter(pixelpoints):
	maximum = 0
	current = 0

	for each1 in pixelpoints:
		for each2 in pixelpoints:
			current = getDistance(each1,each2)
			if maximum <= current:
				maximum = current
			
	return maximum
	
def getRadiusDispersion(pixelpoints,centroid,mean_distance):
	current_distance = 0.0
	sum_distances = 0.0
	if len(pixelpoints)> 1:
		for each in pixelpoints:
			current_distance = getDistance(each, centroid)
			sum_distances = sum_distances + (current_distance-mean_distance)*(current_distance-mean_distance)
		radiusdispersion = math.sqrt(sum_distances/(len(pixelpoints) - 1))
	else:
		radiusdispersion = 0
	return radiusdispersion
	
def getHoles(pixelpoints,centroid,max_distance):
	sum = 0
	for each in pixelpoints:
		sum = sum + max_distance - getDistance(each, centroid)
	holes = sum
	return holes
	
def get2ndEuclideanNorm(pixelpoints,centroid):
	diff = 0
	for each in pixelpoints:
		diff = diff + (each[0]-centroid[0])*(each[0]-centroid[0]) + (each[1]-centroid[1])*(each[1]-centroid[1])
	eN2 = math.sqrt(diff/(len(pixelpoints*2)))
	rms_mean = diff/len(pixelpoints)
	return (eN2,rms_mean)
	
def getComplexity(area,mean_distance_to_boundary):
	if(mean_distance_to_boundary>0):
		return (area/(mean_distance_to_boundary)*(mean_distance_to_boundary));
	else:
		return 0;
		
def getMeanColor(img):
	w,h = img.shape;
	if(w*h>10):
		hist = cv2.calcHist([img],[0],None,[256],[0,256]);
		nb = 0;
		total = 0;
		for i in range(1, 256):
			nb = nb + (i*hist[i][0]);
			total = total+hist[i][0];
		return nb/total;
	else:
		return 0;