import cv2
import numpy as np
import math;

def createGraphe(name,width,height):
	return {'name':str(name),'h':height,'w':width,'channels':[]};
	
def addGraphePoint(graphe,channel,x,y):
	for chann in graphe['channels']:
		if(chann['name']==channel):
			chann['points'].append([x,y]);
	return graphe;
	
def addChannel(graphe,name,color):
	newChannel = {'name':str(name),'points':[],'color':color};
	graphe['channels'].append(newChannel);
	return graphe;
	
def getPoints(graphe,channel):
	return graphe['channels']['points'];

def getDistance(first_point,second_point):
	dx = abs(first_point[0] - second_point[0])
	dy = abs(first_point[1] - second_point[1])
	result = math.sqrt(dx*dx+dy*dy)
	return result;
	
def createGrapheImage(graphe):
	width = graphe['w'];
	height = graphe['h'];
	
	gImage = np.zeros((height+10,width+10,3), np.uint8);
	cv2.rectangle(gImage, (5, 5), (width+5, height+5), (255, 255, 255), 1);
	
	for chann in graphe['channels']:
		xMax = 0.0005;
		yMax = 0.0005;
		
		for point in chann['points']:
			if(point[0]>xMax):
				xMax = point[0];
			if(point[1]>yMax):
				yMax = point[1];
				
		if xMax==0.0005:
			xMax = 1;				
		if yMax==0.0005:
			yMax = 1;
				
		
		for point in chann['points']:
			x = (point[0]*width)/xMax;
			y = (point[1]*height)/yMax;
			
			cv2.circle(gImage, (np.int32(x)+5, np.int32(y)+5), 3, chann['color'], -1);
		
	return gImage;
	
def createGrapheImageGlobal(graphe):
	width = graphe['w'];
	height = graphe['h'];
	
	gImage = np.zeros((height+10,width+10,3), np.uint8);
	cv2.rectangle(gImage, (5, 5), (width+5, height+5), (255, 255, 255), 1);
	
	xMax = 0.0005;
	yMax = 0.0005;
	for chann in graphe['channels']:
		for point in chann['points']:
			if(point[0]>xMax):
				xMax = point[0];
			if(point[1]>yMax):
				yMax = point[1];
				
		if xMax==0.0005:
			xMax = 1;				
		if yMax==0.0005:
			yMax = 1;
				
	for chann in graphe['channels']:
		for point in chann['points']:
			x = (point[0]*width)/xMax;
			y = (point[1]*height)/yMax;
			
			cv2.circle(gImage, (np.int32(x)+5, np.int32(y)+5), 3, chann['color'], -1);
		
	return gImage;